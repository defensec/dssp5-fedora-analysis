## DSSP5 for Fedora analysis

Emacs Org.mode document that can be used to export markdown and html analysis.

      make dssp5-fedora-analysis.html
      make dssp5-fedora-analysis.md

Example output: [**dssp5-fedora-analysis.md**](dssp5-fedora-analysis.md)
