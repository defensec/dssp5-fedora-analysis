# SPDX-FileCopyrightText: © 2021 Dominick Grift <dominick.grift@defensec.nl>
# SPDX-License-Identifier: Unlicense

%.html: %.org
	emacs $< \
		--batch \
		--eval="(setq make-backup-files nil)" \
		--eval="(setq org-confirm-babel-evaluate nil)" \
		--eval="(require 'ob-shell)" \
		-f org-html-export-to-html \
		--kill

%.md: %.org
	emacs $< \
		--batch \
		--eval="(setq make-backup-files nil)" \
		--eval="(setq org-confirm-babel-evaluate nil)" \
		--eval="(require 'ob-shell)" \
		-f org-md-export-to-markdown \
		--kill
