
# Table of Contents

1.  [Introduction](#orgb6539c0)
    1.  [About `dssp5-fedora`](#org9e3922d)
    2.  [About this document](#org095d976)
    3.  [Requirements](#orga20d890)
2.  [Initial Setup](#org108bd93)
    1.  [Export](#orgb3c33ac)
    2.  [Retrieve `dssp5-fedora`](#orgaa3dc01)
    3.  [Build `dssp5-fedora` with defaults](#org39a46d2)
3.  [Overview](#org6bd3305)
    1.  [Statistics](#orgb9f41fd)
    2.  [Sizes](#org599701b)
4.  [Access vectors](#org43fd216)
    1.  [Unknown access vectors](#org9b9dd02)
    2.  [Common access vector permissions](#orgc6dd7f0)
    3.  [Known access vectors](#org721f075)
5.  [Unconfined](#org86defae)
    1.  [Subsystems](#org78a0da5)
    2.  [Domains](#org215da79)



<a id="orgb6539c0"></a>

# Introduction


<a id="org9e3922d"></a>

## About `dssp5-fedora`

DSSP5 for Fedora<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup> is a feature branch of DSSP5<sup><a id="fnr.2" class="footref" href="#fn.2">2</a></sup>.


<a id="org095d976"></a>

## About this document

Dynamically documents properties of DSSP5 for Fedora with Emacs `org.mode`.


<a id="orga20d890"></a>

## Requirements

The following utilities are required to export this document.

-   Coreutils
-   Emacs
-   Findutils
-   Git
-   Policycoreutils
-   Make
-   SECILC
-   SETools
-   Time


<a id="org108bd93"></a>

# Initial Setup


<a id="orgb3c33ac"></a>

## Export

This document can be exported by running `make dssp5-fedora-analysis.html`.


<a id="orgaa3dc01"></a>

## Retrieve `dssp5-fedora`

    git -C dssp5-fedora pull \
        || git clone --single-branch --branch dssp5-fedora \
    	   https://github.com/defensec/dssp5 dssp5-fedora

    Already up to date.


<a id="org39a46d2"></a>

## Build `dssp5-fedora` with defaults

    /usr/bin/time -f \
    	      'Took %e seconds and %M kilobytes of memory to build.' \
    	      make -C dssp5-fedora 2>&1
    exit

    make[1]: Entering directory '/home/kcinimod/Workspace/dssp5-fedora-analysis/dssp5-fedora'
    rm -f policy.33 file_contexts
    secilc -OM true --policyvers=33 src/agent.cil src/agent/hybridagent.cil src/agent/hybridagent/k/kbd.cil src/agent/hybridagent/m/mpg123.cil src/agent/hybridagent/o/openssh.cil src/agent/hybridagent/p/passwd.cil src/agent/hybridagent/s/sasl.cil src/agent/misc/alsa.cil src/agent/misc/dbus/dbus.cil src/agent/misc/dbus/userdbus.cil src/agent/misc/krb5.cil src/agent/misc/libdrm.cil src/agent/misc/libglvnd.cil src/agent/misc/libinput.cil src/agent/misc/mesa.cil src/agent/misc/mpd/mpd.cil src/agent/misc/mpd/usermpd.cil src/agent/misc/nss.cil src/agent/misc/openldap.cil src/agent/misc/pam.cil src/agent/misc/pam/consoleapply.cil src/agent/misc/pam/faillock.cil src/agent/misc/pam/namespacehelper.cil src/agent/misc/pam/timestampcheck.cil src/agent/misc/pam/unixchkpwd.cil src/agent/misc/pam/unixupdate.cil src/agent/misc/pkgmgr.cil src/agent/misc/qemu/qemu.cil src/agent/misc/qemu/userqemu.cil src/agent/misc/samba.cil src/agent/misc/service.cil src/agent/misc/shadowutils/chage.cil src/agent/misc/shadowutils/chpasswd.cil src/agent/misc/shadowutils/groupadd.cil src/agent/misc/shadowutils/useradd.cil src/agent/misc/shadowutils/vipw.cil src/agent/misc/systemd.cil src/agent/misc/systemd/kernelinstall.cil src/agent/misc/systemd/systemdacpower.cil src/agent/misc/systemd/systemdanalyze.cil src/agent/misc/systemd/systemdaskpassword.cil src/agent/misc/systemd/systemdbacklight.cil src/agent/misc/systemd/systemdbinfmt.cil src/agent/misc/systemd/systemdblessboot.cil src/agent/misc/systemd/systemdbootchknofail.cil src/agent/misc/systemd/systemdbusctl.cil src/agent/misc/systemd/systemdcg.cil src/agent/misc/systemd/systemdcgroupsagent.cil src/agent/misc/systemd/systemdcoredump.cil src/agent/misc/systemd/systemdcoredumpctl.cil src/agent/misc/systemd/systemdcryptsetup.cil src/agent/misc/systemd/systemddissect.cil src/agent/misc/systemd/systemdfirstboot.cil src/agent/misc/systemd/systemdfsck.cil src/agent/misc/systemd/systemdhibernateresume.cil src/agent/misc/systemd/systemdhostname.cil src/agent/misc/systemd/systemdhostnamectl.cil src/agent/misc/systemd/systemdhwdb.cil src/agent/misc/systemd/systemdimport.cil src/agent/misc/systemd/systemdinhibit.cil src/agent/misc/systemd/systemdjournal.cil src/agent/misc/systemd/systemdjournalctl.cil src/agent/misc/systemd/systemdlocale.cil src/agent/misc/systemd/systemdlocalectl.cil src/agent/misc/systemd/systemdlogin.cil src/agent/misc/systemd/systemdloginctl.cil src/agent/misc/systemd/systemdmachine.cil src/agent/misc/systemd/systemdmachinectl.cil src/agent/misc/systemd/systemdmachineidsetup.cil src/agent/misc/systemd/systemdmakefs.cil src/agent/misc/systemd/systemdmodulesload.cil src/agent/misc/systemd/systemdmount.cil src/agent/misc/systemd/systemdnetwork.cil src/agent/misc/systemd/systemdnetworkctl.cil src/agent/misc/systemd/systemdnetworkgen.cil src/agent/misc/systemd/systemdnspawn.cil src/agent/misc/systemd/systemdoom.cil src/agent/misc/systemd/systemdoomctl.cil src/agent/misc/systemd/systemdportable.cil src/agent/misc/systemd/systemdportablectl.cil src/agent/misc/systemd/systemdpstore.cil src/agent/misc/systemd/systemdquotacheck.cil src/agent/misc/systemd/systemdrandomseed.cil src/agent/misc/systemd/systemdremountfs.cil src/agent/misc/systemd/systemdrepart.cil src/agent/misc/systemd/systemdreplypassword.cil src/agent/misc/systemd/systemdresolve.cil src/agent/misc/systemd/systemdresolvectl.cil src/agent/misc/systemd/systemdrfkill.cil src/agent/misc/systemd/systemdrun.cil src/agent/misc/systemd/systemdsleep.cil src/agent/misc/systemd/systemdstdiobridge.cil src/agent/misc/systemd/systemdsuloginshell.cil src/agent/misc/systemd/systemdsysctl.cil src/agent/misc/systemd/systemdsysext.cil src/agent/misc/systemd/systemdsystemctl.cil src/agent/misc/systemd/systemdsysusers.cil src/agent/misc/systemd/systemdtimedate.cil src/agent/misc/systemd/systemdtimedatectl.cil src/agent/misc/systemd/systemdtimesync.cil src/agent/misc/systemd/systemdtimewaitsync.cil src/agent/misc/systemd/systemdtmpfiles.cil src/agent/misc/systemd/systemdudev.cil src/agent/misc/systemd/systemdupdatedone.cil src/agent/misc/systemd/systemdupdateutmp.cil src/agent/misc/systemd/systemduserrundir.cil src/agent/misc/systemd/systemdusersessions.cil src/agent/misc/systemd/systemdvconsolesetup.cil src/agent/misc/systemd/systemdveritysetup.cil src/agent/misc/systemd/systemdvolatileroot.cil src/agent/misc/systemd/usersystemd.cil src/agent/misc/systemd/usersystemdrun.cil src/agent/misc/systemd/usersystemdtmpfiles.cil src/agent/misc/tpm2tss.cil src/agent/misc/utillinux/blktools.cil src/agent/misc/utillinux/fsck.cil src/agent/misc/utillinux/fstrim.cil src/agent/misc/utillinux/getty.cil src/agent/misc/utillinux/hwclock.cil src/agent/misc/utillinux/logger.cil src/agent/misc/utillinux/login.cil src/agent/misc/utillinux/mount.cil src/agent/misc/utillinux/parttools.cil src/agent/misc/utillinux/swaptools.cil src/agent/misc/wayland.cil src/agent/misc/wlroots.cil src/agent/misc/xkb.cil src/agent/misc/znc/userznc.cil src/agent/misc/znc/znc.cil src/agent/sysagent.cil src/agent/sysagent/a/alternatives.cil src/agent/sysagent/a/authselect.cil src/agent/sysagent/b/bluez.cil src/agent/sysagent/b/bootloader.cil src/agent/sysagent/b/btrfsprogs.cil src/agent/sysagent/c/certbot.cil src/agent/sysagent/c/cracklib.cil src/agent/sysagent/d/devicemapper.cil src/agent/sysagent/d/dosfstools.cil src/agent/sysagent/d/dovecot.cil src/agent/sysagent/d/dovecotsieve.cil src/agent/sysagent/d/dracut.cil src/agent/sysagent/d/dracutrestore.cil src/agent/sysagent/e/e2fsprogs.cil src/agent/sysagent/g/gitdaemon.cil src/agent/sysagent/h/httpd.cil src/agent/sysagent/i/iproute.cil src/agent/sysagent/i/irqbalance.cil src/agent/sysagent/i/iw.cil src/agent/sysagent/k/kmod.cil src/agent/sysagent/k/knot.cil src/agent/sysagent/l/ldconfig.cil src/agent/sysagent/n/nftables.cil src/agent/sysagent/o/oident.cil src/agent/sysagent/o/opendkim.cil src/agent/sysagent/o/opensshserver.cil src/agent/sysagent/o/osprober.cil src/agent/sysagent/p/polkit.cil src/agent/sysagent/p/postfix.cil src/agent/sysagent/q/quota.cil src/agent/sysagent/q/quotacheck.cil src/agent/sysagent/r/rtkit.cil src/agent/sysagent/s/sysctl.cil src/agent/sysagent/w/wlregd.cil src/agent/sysagent/w/wpasupplicant.cil src/agent/sysagent/z/zramctl.cil src/agent/useragent.cil src/agent/useragent/a/atspi.cil src/agent/useragent/b/brightnessctl.cil src/agent/useragent/c/createrepo.cil src/agent/useragent/c/curl.cil src/agent/useragent/d/dconf.cil src/agent/useragent/e/emacs.cil src/agent/useragent/f/firefox.cil src/agent/useragent/f/foot.cil src/agent/useragent/f/fuse3.cil src/agent/useragent/f/fusesshfs.cil src/agent/useragent/g/gh.cil src/agent/useragent/g/gitemail.cil src/agent/useragent/g/glib.cil src/agent/useragent/g/gnupg.cil src/agent/useragent/g/grim.cil src/agent/useragent/g/gtk3.cil src/agent/useragent/g/gitclient.cil src/agent/useragent/i/irssi.cil src/agent/useragent/k/kitty.cil src/agent/useragent/k/knotutils.cil src/agent/useragent/l/libnotify.cil src/agent/useragent/m/mako.cil src/agent/useragent/m/mozilla.cil src/agent/useragent/m/mpc.cil src/agent/useragent/o/opensshclient.cil src/agent/useragent/o/openssl.cil src/agent/useragent/p/p11kit.cil src/agent/useragent/p/pautils.cil src/agent/useragent/p/pinentry.cil src/agent/useragent/p/pipewire.cil src/agent/useragent/r/rpmsign.cil src/agent/useragent/r/rtorrent.cil src/agent/useragent/s/simplemtpfs.cil src/agent/useragent/s/slurp.cil src/agent/useragent/s/sudo.cil src/agent/useragent/s/sway.cil src/agent/useragent/s/swaybg.cil src/agent/useragent/s/swayidle.cil src/agent/useragent/s/swaylock.cil src/agent/useragent/s/swaysystemd.cil src/agent/useragent/t/tmux.cil src/agent/useragent/t/translateshell.cil src/agent/useragent/u/usersandbox.cil src/agent/useragent/u/utempter.cil src/agent/useragent/w/waypipe.cil src/agent/useragent/w/wfrecorder.cil src/agent/useragent/w/wlclipboard.cil src/dev.cil src/dev/nodedev.cil src/dev/nodedev/apmnodedev.cil src/dev/nodedev/autofsnodedev.cil src/dev/nodedev/btrfscontrolnodedev.cil src/dev/nodedev/cachefilesnodedev.cil src/dev/nodedev/cdcwdmnodedev.cil src/dev/nodedev/clocknodedev.cil src/dev/nodedev/cpunodedev.cil src/dev/nodedev/crashnodedev.cil src/dev/nodedev/cusenodedev.cil src/dev/nodedev/dmaheapnodedev.cil src/dev/nodedev/dmcontrolnodedev.cil src/dev/nodedev/drinodedev.cil src/dev/nodedev/drmdpauxnodedev.cil src/dev/nodedev/eventnodedev.cil src/dev/nodedev/fbnodedev.cil src/dev/nodedev/gpionodedev.cil src/dev/nodedev/hiddevnodedev.cil src/dev/nodedev/hidrawnodedev.cil src/dev/nodedev/hwrngnodedev.cil src/dev/nodedev/i2cnodedev.cil src/dev/nodedev/iionodedev.cil src/dev/nodedev/infinibandnodedev.cil src/dev/nodedev/inputnodedev.cil src/dev/nodedev/ipminodedev.cil src/dev/nodedev/kfdnodedev.cil src/dev/nodedev/kmsgnodedev.cil src/dev/nodedev/ksmnodedev.cil src/dev/nodedev/kvmnodedev.cil src/dev/nodedev/lircnodedev.cil src/dev/nodedev/loopcontrolnodedev.cil src/dev/nodedev/mcelognodedev.cil src/dev/nodedev/meinodedev.cil src/dev/nodedev/memnodedev.cil src/dev/nodedev/modemnodedev.cil src/dev/nodedev/ndctlnodedev.cil src/dev/nodedev/nullnodedev.cil src/dev/nodedev/nvramnodedev.cil src/dev/nodedev/pmunodedev.cil src/dev/nodedev/pppnodedev.cil src/dev/nodedev/printernodedev.cil src/dev/nodedev/ptmxnodedev.cil src/dev/nodedev/qosnodedev.cil src/dev/nodedev/randomnodedev.cil src/dev/nodedev/rfkillnodedev.cil src/dev/nodedev/sndnodedev.cil src/dev/nodedev/tpmnodedev.cil src/dev/nodedev/ttynodedev.cil src/dev/nodedev/tuntapnodedev.cil src/dev/nodedev/udmabufnodedev.cil src/dev/nodedev/uhidnodedev.cil src/dev/nodedev/uinputnodedev.cil src/dev/nodedev/uionodedev.cil src/dev/nodedev/usbmonnodedev.cil src/dev/nodedev/usbnodedev.cil src/dev/nodedev/v4lnodedev.cil src/dev/nodedev/vfionodedev.cil src/dev/nodedev/vgaarbiternodedev.cil src/dev/nodedev/vhostnodedev.cil src/dev/nodedev/vmcinodedev.cil src/dev/nodedev/watchdognodedev.cil src/dev/nodedev/zeronodedev.cil src/dev/stordev.cil src/dev/stordev/dmstordev.cil src/dev/stordev/fusestordev.cil src/dev/stordev/hdstordev.cil src/dev/stordev/loopstordev.cil src/dev/stordev/mdstordev.cil src/dev/stordev/mtdstordev.cil src/dev/stordev/nbdstordev.cil src/dev/stordev/nvmestordev.cil src/dev/stordev/rawstordev.cil src/dev/stordev/removablestordev.cil src/dev/stordev/sdstordev.cil src/dev/stordev/sgstordev.cil src/dev/stordev/vdstordev.cil src/dev/stordev/xdstordev.cil src/dev/stordev/zramstordev.cil src/dev/termdev.cil src/dev/termdev/ptytermdev.cil src/dev/termdev/ptytermdev/loginptytermdev.cil src/dev/termdev/serialtermdev.cil src/dev/termdev/serialtermdev/acmserialtermdev.cil src/dev/termdev/serialtermdev/consoleserialtermdev.cil src/dev/termdev/serialtermdev/loginserialtermdev.cil src/dev/termdev/serialtermdev/loginserialtermdev/ttyloginserialtermdev.cil src/dev/termdev/serialtermdev/msmserialtermdev.cil src/dev/termdev/serialtermdev/usbserialtermdev.cil src/dev/termdev/serialtermdev/vcsserialtermdev.cil src/dev/termdev/serialtermdev/vportserialtermdev.cil src/dev/termdev/systermdev.cil src/dev/termdev/systermdev/sysloginptytermdev.cil src/dev/termdev/systermdev/sysserialtermdev.cil src/dev/termdev/usertermdev.cil src/dev/termdev/usertermdev/userptytermdev.cil src/dev/termdev/usertermdev/userserialtermdev.cil src/file.cil src/file/authfile.cil src/file/authfile/shadowauthfile.cil src/file/bootfile.cil src/file/bootflagfile.cil src/file/certfile.cil src/file/conffile.cil src/file/conffile/aliasesconffile.cil src/file/conffile/autoconffile.cil src/file/conffile/bootparamsconffile.cil src/file/conffile/cryptopolconffile.cil src/file/conffile/dconffile.cil src/file/conffile/editorconffile.cil src/file/conffile/environconffile.cil src/file/conffile/ethersconffile.cil src/file/conffile/fontconffile.cil src/file/conffile/fstabconffile.cil src/file/conffile/gcryptconffile.cil src/file/conffile/hostsconffile.cil src/file/conffile/inputrcconffile.cil src/file/conffile/libnlconffile.cil src/file/conffile/localeconffile.cil src/file/conffile/logindefsconffile.cil src/file/conffile/mailconffile.cil src/file/conffile/manconffile.cil src/file/conffile/mimeconffile.cil src/file/conffile/motdconffile.cil src/file/conffile/netgroupconffile.cil src/file/conffile/netmasksconffile.cil src/file/conffile/networkconffile.cil src/file/conffile/networksconffile.cil src/file/conffile/protocolsconffile.cil src/file/conffile/publickeyconffile.cil src/file/conffile/resolvconffile.cil src/file/conffile/rpcconffile.cil src/file/conffile/selinuxconffile.cil src/file/conffile/servicesconffile.cil src/file/conffile/shellsconffile.cil src/file/conffile/terminfoconffile.cil src/file/conffile/x11conffile.cil src/file/conffile/xdgconffile.cil src/file/contentfile.cil src/file/contentfile/gitcontentfile.cil src/file/contentfile/gitwebcontentfile.cil src/file/contentfile/webcontentfile.cil src/file/datafile.cil src/file/datafile/backgroundsdatafile.cil src/file/datafile/cryptopoldatafile.cil src/file/datafile/environdatafile.cil src/file/datafile/execfile.cil src/file/datafile/execfile/shellexecfile.cil src/file/datafile/factorydatafile.cil src/file/datafile/firmwaredatafile.cil src/file/datafile/fontdatafile.cil src/file/datafile/hardwaredatafile.cil src/file/datafile/iconsdatafile.cil src/file/datafile/libfile.cil src/file/datafile/localedatafile.cil src/file/datafile/mandatafile.cil src/file/datafile/mimedatafile.cil src/file/datafile/modfile.cil src/file/datafile/motddatafile.cil src/file/datafile/perl5datafile.cil src/file/datafile/pixmapsdatafile.cil src/file/datafile/srcfile.cil src/file/datafile/terminfodatafile.cil src/file/datafile/themesdatafile.cil src/file/datafile/x11datafile.cil src/file/devfile.cil src/file/homefile.cil src/file/homefile/syshomefile.cil src/file/hugetlbfsfile.cil src/file/hugetlbfsfile/syshugetlbfsfile.cil src/file/misc/initctlfile.cil src/file/misc/lostfoundfile.cil src/file/misc/machineidfile.cil src/file/misc/mediafile.cil src/file/misc/nologinfile.cil src/file/misc/passwdfile.cil src/file/misc/pwdlockfile.cil src/file/misc/rootfile.cil src/file/misc/unknownfile.cil src/file/mqueuefsfile.cil src/file/mqueuefsfile/sysmqueuefsfile.cil src/file/runfile.cil src/file/runfile/environrunfile.cil src/file/runfile/motdrunfile.cil src/file/runfile/runlockfile.cil src/file/runfile/runuserfile.cil src/file/runfile/utmprunfile.cil src/file/secfile.cil src/file/secfile/selinuxsecfile.cil src/file/tmpfile.cil src/file/tmpfile/systmpfile.cil src/file/tmpfsfile.cil src/file/tmpfsfile/systmpfsfile.cil src/file/userfile.cil src/file/userfile/usercontentfile.cil src/file/userfile/usercontentfile/usergitcontentfile.cil src/file/userfile/usercontentfile/userwebcontentfile.cil src/file/userfile/userhomefile.cil src/file/userfile/userhomefile/inputrchomefile.cil src/file/userfile/userhomefile/mailhomefile.cil src/file/userfile/userhomefile/mediahomefile.cil src/file/userfile/userhomefile/shellrchomefile.cil src/file/userfile/userhomefile/sievehomefile.cil src/file/userfile/userhomefile/userhomecachefile.cil src/file/userfile/userhomefile/userhomecachefile/fonthomecachefile.cil src/file/userfile/userhomefile/userhomeconffile.cil src/file/userfile/userhomefile/userhomeconffile/environhomeconffile.cil src/file/userfile/userhomefile/userhomeconffile/fonthomeconffile.cil src/file/userfile/userhomefile/userhomeconffile/mimehomeconffile.cil src/file/userfile/userhomefile/userhomedatafile.cil src/file/userfile/userhomefile/userhomedatafile/certhomedatafile.cil src/file/userfile/userhomefile/userhomedatafile/exechomefile.cil src/file/userfile/userhomefile/userhomedatafile/fonthomedatafile.cil src/file/userfile/userhomefile/userhomedatafile/libhomefile.cil src/file/userfile/userhomefile/userhomedatafile/mimehomedatafile.cil src/file/userfile/usermailfile.cil src/file/userfile/usermqueuefsfile.cil src/file/userfile/userrunuserfile.cil src/file/userfile/usertmpfile.cil src/file/userfile/usertmpfsfile.cil src/file/varfile.cil src/file/varfile/cachefile.cil src/file/varfile/cachefile/fontcachefile.cil src/file/varfile/dbfile.cil src/file/varfile/logfile.cil src/file/varfile/logfile/utmplogfile.cil src/file/varfile/spoolfile.cil src/file/varfile/spoolfile/mailfile.cil src/file/varfile/statefile.cil src/file/varfile/statefile/selinuxstatefile.cil src/fs.cil src/fs/noseclabelfs.cil src/fs/noseclabelfs/aionoseclabelfs.cil src/fs/noseclabelfs/anoninodenoseclabelfs.cil src/fs/noseclabelfs/autonoseclabelfs.cil src/fs/noseclabelfs/bdevnoseclabelfs.cil src/fs/noseclabelfs/binfmtmiscnoseclabelfs.cil src/fs/noseclabelfs/bpfnoseclabelfs.cil src/fs/noseclabelfs/cinoseclabelfs.cil src/fs/noseclabelfs/confignoseclabelfs.cil src/fs/noseclabelfs/cpusetnoseclabelfs.cil src/fs/noseclabelfs/dosnoseclabelfs.cil src/fs/noseclabelfs/drmnoseclabelfs.cil src/fs/noseclabelfs/efivarnoseclabelfs.cil src/fs/noseclabelfs/fusenoseclabelfs.cil src/fs/noseclabelfs/iso9660noseclabelfs.cil src/fs/noseclabelfs/nfsdnoseclabelfs.cil src/fs/noseclabelfs/nfsnoseclabelfs.cil src/fs/noseclabelfs/nsnoseclabelfs.cil src/fs/noseclabelfs/procnoseclabelfs.cil src/fs/noseclabelfs/removablenoseclabelfs.cil src/fs/noseclabelfs/rpcpipenoseclabelfs.cil src/fs/noseclabelfs/securitynoseclabelfs.cil src/fs/noseclabelfs/selinuxnoseclabelfs.cil src/fs/seclabelfs.cil src/fs/seclabelfs/cgroupseclabelfs.cil src/fs/seclabelfs/debugseclabelfs.cil src/fs/seclabelfs/devptsseclabelfs.cil src/fs/seclabelfs/devtmpseclabelfs.cil src/fs/seclabelfs/eventpollseclabelfs.cil src/fs/seclabelfs/hugetlbseclabelfs.cil src/fs/seclabelfs/mqueueseclabelfs.cil src/fs/seclabelfs/pipeseclabelfs.cil src/fs/seclabelfs/pstoreseclabelfs.cil src/fs/seclabelfs/rootseclabelfs.cil src/fs/seclabelfs/sockseclabelfs.cil src/fs/seclabelfs/sysseclabelfs.cil src/fs/seclabelfs/tmpseclabelfs.cil src/fs/seclabelfs/traceseclabelfs.cil src/fs/seclabelfs/xattrseclabelfs.cil src/invalid.cil src/misc.cil src/misc/av.cil src/misc/av/anoninodeav.cil src/misc/av/binderav.cil src/misc/av/bpfav.cil src/misc/av/capabilityav.cil src/misc/av/fdav.cil src/misc/av/ipcav.cil src/misc/av/kernelserviceav.cil src/misc/av/keyav.cil src/misc/av/lockdownav.cil src/misc/av/memprotectav.cil src/misc/av/msgav.cil src/misc/av/passwdav.cil src/misc/av/perfeventav.cil src/misc/av/socketav.cil src/misc/av/systemav.cil src/misc/conf.cil src/misc/constrain/ibac.cil src/misc/constrain/mcs.cil src/misc/constrain/rbac.cil src/misc/constrain/rbacsep.cil src/misc/default.cil src/misc/isid.cil src/misc/map.cil src/misc/mls.cil src/misc/obj.cil src/misc/perm.cil src/misc/unconfined.cil src/net.cil src/net/ibnet.cil src/net/ibnet/endportibnet.cil src/net/ibnet/pkeyibnet.cil src/net/netifnet.cil src/net/nodenet.cil src/net/packetnet.cil src/net/peernet.cil src/net/portnet.cil src/net/portnet/ephemeralportnet.cil src/net/portnet/reservedportnet.cil src/net/portnet/reservedportnet/d/dhcpreservedport.cil src/net/portnet/reservedportnet/d/dnsreservedport.cil src/net/portnet/reservedportnet/h/httpreservedport.cil src/net/portnet/reservedportnet/i/identreservedport.cil src/net/portnet/reservedportnet/i/imapreservedport.cil src/net/portnet/reservedportnet/i/ippreservedport.cil src/net/portnet/reservedportnet/k/krb5reservedport.cil src/net/portnet/reservedportnet/l/ldapreservedport.cil src/net/portnet/reservedportnet/n/nntpreservedport.cil src/net/portnet/reservedportnet/p/popreservedport.cil src/net/portnet/reservedportnet/s/sambareservedport.cil src/net/portnet/reservedportnet/s/smtpreservedport.cil src/net/portnet/reservedportnet/s/sshreservedport.cil src/net/portnet/unreservedportnet.cil src/net/portnet/unreservedportnet/g/gitunreservedport.cil src/net/portnet/unreservedportnet/h/hkpunreservedport.cil src/net/portnet/unreservedportnet/i/ircunreservedport.cil src/net/portnet/unreservedportnet/l/llmnrunreservedport.cil src/net/portnet/unreservedportnet/m/mdnsunreservedport.cil src/net/portnet/unreservedportnet/m/mfcsunreservedport.cil src/net/portnet/unreservedportnet/m/mpdunreservedport.cil src/net/portnet/unreservedportnet/n/nbdunreservedport.cil src/net/portnet/unreservedportnet/o/opendkimunreservedport.cil src/net/portnet/unreservedportnet/p/privoxyunreservedport.cil src/net/portnet/unreservedportnet/s/shoutunreservedport.cil src/net/portnet/unreservedportnet/s/sieveunreservedport.cil src/net/portnet/unreservedportnet/s/smtpunreservedport.cil src/net/portnet/unreservedportnet/s/sudologsrvunreservedport.cil src/net/portnet/unreservedportnet/t/torrentunreservedport.cil src/net/portnet/unreservedportnet/t/torunreservedport.cil src/net/portnet/unreservedportnet/v/vncunreservedport.cil src/net/spdnet.cil src/selinux.cil src/selinux/booleanfile.cil src/selinux/booleanfile/invalidassociationsbooleanfile.cil src/selinux/booleanfile/invalidpacketsbooleanfile.cil src/selinux/booleanfile/invalidpeersbooleanfile.cil src/selinux/booleanfile/systemdnspawnbinduserbooleanfile.cil src/subj.cil src/sys.cil src/sys/cgroupfile.cil src/sys/debugfile.cil src/sys/procfile.cil src/sys/procfile/acpiprocfile.cil src/sys/procfile/asoundprocfile.cil src/sys/procfile/bootconfigprocfile.cil src/sys/procfile/buddyinfoprocfile.cil src/sys/procfile/busprocfile.cil src/sys/procfile/cgroupsprocfile.cil src/sys/procfile/cmdlineprocfile.cil src/sys/procfile/consolesprocfile.cil src/sys/procfile/cpuinfoprocfile.cil src/sys/procfile/cpuprocfile.cil src/sys/procfile/cryptoprocfile.cil src/sys/procfile/devicesprocfile.cil src/sys/procfile/diskstatsprocfile.cil src/sys/procfile/dmaprocfile.cil src/sys/procfile/driverprocfile.cil src/sys/procfile/dynamicdebugprocfile.cil src/sys/procfile/execdomainsprocfile.cil src/sys/procfile/fbprocfile.cil src/sys/procfile/filesystemsprocfile.cil src/sys/procfile/fsprocfile.cil src/sys/procfile/interruptsprocfile.cil src/sys/procfile/iomemprocfile.cil src/sys/procfile/ioportsprocfile.cil src/sys/procfile/irqprocfile.cil src/sys/procfile/jffs2bbcprocfile.cil src/sys/procfile/kallsymsprocfile.cil src/sys/procfile/kcoreprocfile.cil src/sys/procfile/keysprocfile.cil src/sys/procfile/keyusersprocfile.cil src/sys/procfile/kmsgprocfile.cil src/sys/procfile/kpagecgroupprocfile.cil src/sys/procfile/kpagecountprocfile.cil src/sys/procfile/kpageflagsprocfile.cil src/sys/procfile/latencystatsprocfile.cil src/sys/procfile/loadavgprocfile.cil src/sys/procfile/lockdepchainsprocfile.cil src/sys/procfile/lockdepprocfile.cil src/sys/procfile/lockdepstatsprocfile.cil src/sys/procfile/locksprocfile.cil src/sys/procfile/lockstatprocfile.cil src/sys/procfile/mdstatprocfile.cil src/sys/procfile/meminfoprocfile.cil src/sys/procfile/miscprocfile.cil src/sys/procfile/modulesprocfile.cil src/sys/procfile/mptprocfile.cil src/sys/procfile/mtdprocfile.cil src/sys/procfile/mtrrprocfile.cil src/sys/procfile/netprocfile.cil src/sys/procfile/pagetypeinfoprocfile.cil src/sys/procfile/partitionsprocfile.cil src/sys/procfile/pressureprocfile.cil src/sys/procfile/scheddebugprocfile.cil src/sys/procfile/schedstatprocfile.cil src/sys/procfile/scsiprocfile.cil src/sys/procfile/slabinfoprocfile.cil src/sys/procfile/softirqsprocfile.cil src/sys/procfile/statprocfile.cil src/sys/procfile/swapsprocfile.cil src/sys/procfile/sysctlfile.cil src/sys/procfile/sysctlfile/abisysctlfile.cil src/sys/procfile/sysctlfile/cryptosysctlfile.cil src/sys/procfile/sysctlfile/debugsysctlfile.cil src/sys/procfile/sysctlfile/devsysctlfile.cil src/sys/procfile/sysctlfile/fssysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/caplastcapkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/corepatternkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/corepipelimitkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/firmwareconfigkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/hostnamekernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/keyskernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/modprobekernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/nslastpidkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/osreleasekernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/overflowuidkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/pidmaxkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/poweroffcmdkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/ptykernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/randomkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/seccompkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/threadsmaxkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/usermodehelperkernelsysctlfile.cil src/sys/procfile/sysctlfile/kernelsysctlfile/yamakernelsysctlfile.cil src/sys/procfile/sysctlfile/netsysctlfile.cil src/sys/procfile/sysctlfile/netsysctlfile/corenetsysctlfile.cil src/sys/procfile/sysctlfile/netsysctlfile/ipv4netsysctlfile.cil src/sys/procfile/sysctlfile/netsysctlfile/ipv6netsysctlfile.cil src/sys/procfile/sysctlfile/netsysctlfile/mptcpnetsysctlfile.cil src/sys/procfile/sysctlfile/netsysctlfile/netfilternetsysctlfile.cil src/sys/procfile/sysctlfile/netsysctlfile/unixnetsysctlfile.cil src/sys/procfile/sysctlfile/sunrpcsysctlfile.cil src/sys/procfile/sysctlfile/usersysctlfile.cil src/sys/procfile/sysctlfile/vmsysctlfile.cil src/sys/procfile/sysctlfile/vmsysctlfile/overcommitmemoryvmsysctlfile.cil src/sys/procfile/sysctlprocfile.cil src/sys/procfile/sysrqtriggerprocfile.cil src/sys/procfile/sysvipcprocfile.cil src/sys/procfile/timerlistprocfile.cil src/sys/procfile/ttyprocfile.cil src/sys/procfile/uptimeprocfile.cil src/sys/procfile/versionprocfile.cil src/sys/procfile/vmallocprocfile.cil src/sys/procfile/vmstatprocfile.cil src/sys/procfile/zoneinfoprocfile.cil src/sys/sysfile.cil src/sys/sysfile/blocksysfile.cil src/sys/sysfile/bussysfile.cil src/sys/sysfile/classsysfile.cil src/sys/sysfile/classsysfile/gpioclasssysfile.cil src/sys/sysfile/classsysfile/ledsclasssysfile.cil src/sys/sysfile/classsysfile/zramclasssysfile.cil src/sys/sysfile/devicessysfile.cil src/sys/sysfile/devicessysfile/cpudevicessysfile.cil src/sys/sysfile/devicessysfile/ledsdevicessysfile.cil src/sys/sysfile/devicessysfile/memorydevicessysfile.cil src/sys/sysfile/devicessysfile/nodedevicessysfile.cil src/sys/sysfile/devicessysfile/zramdevicessysfile.cil src/sys/sysfile/devsysfile.cil src/sys/sysfile/firmwaresysfile.cil src/sys/sysfile/fssysfile.cil src/sys/sysfile/fssysfile/btrfssysfile.cil src/sys/sysfile/fssysfile/ext4fssysfile.cil src/sys/sysfile/fssysfile/f2fssysfile.cil src/sys/sysfile/fssysfile/fusefssysfile.cil src/sys/sysfile/hypervisorsysfile.cil src/sys/sysfile/kernelsysfile.cil src/sys/sysfile/kernelsysfile/ksmkernelsysfile.cil src/sys/sysfile/modulesysfile.cil src/sys/sysfile/powersysfile.cil src/sys/tracefile.cil src/unlabeled.cil src/user.cil src/user/gitshelluser.cil src/user/privuser.cil src/user/sysuser.cil src/user/unprivuser.cil src/user/wheeluser.cil
    setfiles -c policy.33 file_contexts
    make[1]: Leaving directory '/home/kcinimod/Workspace/dssp5-fedora-analysis/dssp5-fedora'
    Took 2.83 seconds and 233120 kilobytes of memory to build.


<a id="org6bd3305"></a>

# Overview


<a id="orgb9f41fd"></a>

## Statistics

These are the overall statistics.

    seinfo dssp5-fedora/policy.*

    Statistics for policy file: dssp5-fedora/policy.33
    Policy Version:             33 (MLS enabled)
    Target Policy:              selinux
    Handle unknown classes:     allow
      Classes:              98    Permissions:         255
      Sensitivities:         1    Categories:         1024
      Types:              1611    Attributes:          508
      Users:                 4    Roles:                 5
      Booleans:              0    Cond. Expr.:           0
      Allow:              9718    Neverallow:            0
      Auditallow:            0    Dontaudit:           692
      Type_trans:         4683    Type_change:          17
      Type_member:           0    Range_trans:           0
      Role allow:            4    Role_trans:            0
      Constraints:         623    Validatetrans:         0
      MLS Constrain:       497    MLS Val. Tran:         0
      Permissives:           0    Polcap:                7
      Defaults:             14    Typebounds:            1
      Allowxperm:           18    Neverallowxperm:       0
      Auditallowxperm:       0    Dontauditxperm:        0
      Ibendportcon:          0    Ibpkeycon:             0
      Initial SIDs:          9    Fs_use:               38
      Genfscon:            173    Portcon:             188
      Netifcon:              0    Nodecon:               0


<a id="org599701b"></a>

## Sizes

Size of the policy.

    stat -c 'file %n is %s bytes in size.' dssp5-fedora/policy.*

    file dssp5-fedora/policy.33 is 1021868 bytes in size.

Size of file contexts.

    stat -c 'file %n is %s bytes in size.' dssp5-fedora/file_contexts

    file dssp5-fedora/file_contexts is 156844 bytes in size.


<a id="org43fd216"></a>

# Access vectors<sup><a id="fnr.3" class="footref" href="#fn.3">3</a></sup>


<a id="org9b9dd02"></a>

## Unknown access vectors

The default action to take when unknown access vectors are encountered.

    seinfo dssp5-fedora/policy.* | grep "^Handle unknown classes:"

    Handle unknown classes:     allow


<a id="orgc6dd7f0"></a>

## Common access vector permissions

Known access vector permissions that are shared between by security classes.

    seinfo dssp5-fedora/policy.* --flat --common -x

    common common_capability
    {
    	fsetid
    	setgid
    	audit_write
    	dac_read_search
    	net_raw
    	audit_control
    	sys_tty_config
    	ipc_lock
    	sys_time
    	sys_ptrace
    	fowner
    	sys_pacct
    	mknod
    	net_admin
    	chown
    	net_bind_service
    	kill
    	lease
    	sys_rawio
    	setpcap
    	sys_resource
    	setuid
    	dac_override
    	sys_chroot
    	net_broadcast
    	sys_admin
    	sys_module
    	sys_boot
    	sys_nice
    	ipc_owner
    	setfcap
    	linux_immutable
    }
    common common_capability2
    {
    	syslog
    	block_suspend
    	audit_read
    	checkpoint_restore
    	mac_override
    	perfmon
    	bpf
    	mac_admin
    	wake_alarm
    }
    common common_file
    {
    	relabelto
    	link
    	append
    	watch_mount
    	write
    	quotaon
    	open
    	audit_access
    	rename
    	relabelfrom
    	execute
    	read
    	watch_sb
    	getattr
    	ioctl
    	watch
    	create
    	lock
    	mounton
    	watch_with_perm
    	map
    	watch_reads
    	unlink
    	setattr
    	execmod
    }
    common common_ipc
    {
    	unix_write
    	setattr
    	associate
    	unix_read
    	write
    	destroy
    	create
    	read
    	getattr
    }
    common common_socket
    {
    	listen
    	relabelto
    	setopt
    	append
    	write
    	accept
    	name_bind
    	relabelfrom
    	read
    	getattr
    	recvfrom
    	ioctl
    	getopt
    	connect
    	create
    	lock
    	sendto
    	bind
    	map
    	setattr
    	shutdown
    }


<a id="org721f075"></a>

## Known access vectors

Known access vector permissions and their associated known security classes.

    seinfo dssp5-fedora/policy.* --flat -xc

    class alg_socket
    inherits common_socket
    
    class anon_inode
    inherits common_file
    
    class appletalk_socket
    inherits common_socket
    
    class association
    {
    	sendto
    	setcontext
    	recvfrom
    	polmatch
    }
    class atmpvc_socket
    inherits common_socket
    
    class atmsvc_socket
    inherits common_socket
    
    class ax25_socket
    inherits common_socket
    
    class binder
    {
    	set_context_mgr
    	transfer
    	impersonate
    	call
    }
    class blk_file
    inherits common_file
    
    class bluetooth_socket
    inherits common_socket
    
    class bpf
    {
    	prog_load
    	prog_run
    	map_write
    	map_create
    	map_read
    }
    class caif_socket
    inherits common_socket
    
    class can_socket
    inherits common_socket
    
    class cap2_userns
    inherits common_capability2
    
    class cap_userns
    inherits common_capability
    
    class capability
    inherits common_capability
    
    class capability2
    inherits common_capability2
    
    class chr_file
    inherits common_file
    
    class dbus
    {
    	send_msg
    	acquire_svc
    }
    class dccp_socket
    inherits common_socket
    {
    	name_connect
    	node_bind
    }
    class decnet_socket
    inherits common_socket
    
    class dir
    inherits common_file
    {
    	add_name
    	search
    	rmdir
    	remove_name
    	reparent
    }
    class fd
    {
    	use
    }
    class fifo_file
    inherits common_file
    
    class file
    inherits common_file
    {
    	execute_no_trans
    	entrypoint
    }
    class filesystem
    {
    	remount
    	quotamod
    	relabelto
    	unmount
    	getattr
    	watch
    	relabelfrom
    	mount
    	associate
    	quotaget
    }
    class icmp_socket
    inherits common_socket
    {
    	node_bind
    }
    class ieee802154_socket
    inherits common_socket
    
    class infiniband_endport
    {
    	manage_subnet
    }
    class infiniband_pkey
    {
    	access
    }
    class ipc
    inherits common_ipc
    
    class ipx_socket
    inherits common_socket
    
    class irda_socket
    inherits common_socket
    
    class isdn_socket
    inherits common_socket
    
    class iucv_socket
    inherits common_socket
    
    class kcm_socket
    inherits common_socket
    
    class kernel_service
    {
    	create_files_as
    	use_as_override
    }
    class key
    {
    	read
    	link
    	search
    	create
    	write
    	setattr
    	view
    }
    class key_socket
    inherits common_socket
    
    class llc_socket
    inherits common_socket
    
    class lnk_file
    inherits common_file
    
    class lockdown
    {
    	integrity
    	confidentiality
    }
    class memprotect
    {
    	mmap_zero
    }
    class msg
    {
    	receive
    	send
    }
    class msgq
    inherits common_ipc
    {
    	enqueue
    }
    class netif
    {
    	egress
    	ingress
    }
    class netlink_audit_socket
    inherits common_socket
    {
    	nlmsg_readpriv
    	nlmsg_read
    	nlmsg_write
    	nlmsg_tty_audit
    	nlmsg_relay
    }
    class netlink_connector_socket
    inherits common_socket
    
    class netlink_crypto_socket
    inherits common_socket
    
    class netlink_dnrt_socket
    inherits common_socket
    
    class netlink_fib_lookup_socket
    inherits common_socket
    
    class netlink_generic_socket
    inherits common_socket
    
    class netlink_iscsi_socket
    inherits common_socket
    
    class netlink_kobject_uevent_socket
    inherits common_socket
    
    class netlink_netfilter_socket
    inherits common_socket
    
    class netlink_nflog_socket
    inherits common_socket
    
    class netlink_rdma_socket
    inherits common_socket
    
    class netlink_route_socket
    inherits common_socket
    {
    	nlmsg_write
    	nlmsg_read
    }
    class netlink_scsitransport_socket
    inherits common_socket
    
    class netlink_selinux_socket
    inherits common_socket
    
    class netlink_socket
    inherits common_socket
    
    class netlink_tcpdiag_socket
    inherits common_socket
    {
    	nlmsg_write
    	nlmsg_read
    }
    class netlink_xfrm_socket
    inherits common_socket
    {
    	nlmsg_write
    	nlmsg_read
    }
    class netrom_socket
    inherits common_socket
    
    class nfc_socket
    inherits common_socket
    
    class node
    {
    	sendto
    	recvfrom
    }
    class packet
    {
    	forward_in
    	recv
    	send
    	relabelto
    	forward_out
    }
    class packet_socket
    inherits common_socket
    
    class passwd
    {
    	passwd
    	chsh
    	chfn
    	rootok
    	crontab
    }
    class peer
    {
    	recv
    }
    class perf_event
    {
    	read
    	tracepoint
    	open
    	write
    	kernel
    	cpu
    }
    class phonet_socket
    inherits common_socket
    
    class pppox_socket
    inherits common_socket
    
    class process
    {
    	noatsecure
    	dyntransition
    	setrlimit
    	setcurrent
    	getsched
    	sigkill
    	setexec
    	getpgid
    	fork
    	rlimitinh
    	getsession
    	signull
    	setsockcreate
    	getattr
    	getcap
    	siginh
    	signal
    	setsched
    	setfscreate
    	share
    	setpgid
    	execheap
    	transition
    	execmem
    	execstack
    	setcap
    	sigchld
    	ptrace
    	setkeycreate
    	sigstop
    	getrlimit
    }
    class process2
    {
    	nosuid_transition
    	nnp_transition
    }
    class qipcrtr_socket
    inherits common_socket
    
    class rawip_socket
    inherits common_socket
    {
    	node_bind
    }
    class rds_socket
    inherits common_socket
    
    class rose_socket
    inherits common_socket
    
    class rxrpc_socket
    inherits common_socket
    
    class sctp_socket
    inherits common_socket
    {
    	name_connect
    	node_bind
    	association
    }
    class security
    {
    	read_policy
    	compute_user
    	compute_create
    	compute_member
    	compute_relabel
    	validate_trans
    	setenforce
    	check_context
    	load_policy
    	setsecparam
    	setcheckreqprot
    	compute_av
    	setbool
    }
    class sem
    inherits common_ipc
    
    class service
    {
    	reload
    	enable
    	stop
    	start
    	disable
    	status
    }
    class shm
    inherits common_ipc
    {
    	lock
    }
    class smc_socket
    inherits common_socket
    
    class sock_file
    inherits common_file
    
    class socket
    inherits common_socket
    
    class system
    {
    	module_request
    	reload
    	halt
    	syslog_console
    	reboot
    	ipc_info
    	syslog_mod
    	module_load
    	stop
    	start
    	syslog_read
    	status
    }
    class tcp_socket
    inherits common_socket
    {
    	name_connect
    	node_bind
    }
    class tipc_socket
    inherits common_socket
    
    class tun_socket
    inherits common_socket
    {
    	attach_queue
    }
    class udp_socket
    inherits common_socket
    {
    	node_bind
    }
    class unix_dgram_socket
    inherits common_socket
    
    class unix_stream_socket
    inherits common_socket
    {
    	connectto
    }
    class vsock_socket
    inherits common_socket
    
    class x25_socket
    inherits common_socket
    
    class xdp_socket
    inherits common_socket


<a id="org86defae"></a>

# Unconfined


<a id="org78a0da5"></a>

## Subsystems

Unconfined TE allow rules associated with subsystem members (if any).

    for typeattr in `seinfo dssp5-fedora/policy.* --flat -a \
    | grep unconfined.typeattr`
    do
        echo "Subsystem ${typeattr%%.*}:"
        sesearch dssp5-fedora/policy.* -A -s $typeattr -ds
        sesearch dssp5-fedora/policy.* -A -t $typeattr -dt
        echo
    done

    Subsystem booleanfile:
    allow booleanfile.unconfined.typeattr booleanfile.typeattr:file { append audit_access create execute execute_no_trans getattr ioctl link lock map mounton open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    
    Subsystem cgroupfile:
    
    Subsystem dbus:
    allow dbus.unconfined.typeattr dbus.unconfined.typeattr:dbus send_msg;
    allow dbus.unconfined.typeattr dbus.unconfined.typeattr:dbus send_msg;
    
    Subsystem debugfile:
    
    Subsystem dev:
    allow dev.unconfined.typeattr dev.typeattr:blk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow dev.unconfined.typeattr dev.typeattr:chr_file { append audit_access create execute getattr ioctl link lock map open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    
    Subsystem file:
    allow file.unconfined.typeattr file.typeattr:blk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow file.unconfined.typeattr file.typeattr:chr_file { append audit_access create execute getattr ioctl link lock map open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow file.unconfined.typeattr file.typeattr:dir { add_name append audit_access create execute getattr ioctl link lock map mounton open quotaon read relabelfrom relabelto remove_name rename reparent rmdir search setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow file.unconfined.typeattr file.typeattr:fifo_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow file.unconfined.typeattr file.typeattr:file { append audit_access create execute execute_no_trans getattr ioctl link lock map mounton open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow file.unconfined.typeattr file.typeattr:kernel_service create_files_as;
    allow file.unconfined.typeattr file.typeattr:lnk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow file.unconfined.typeattr file.typeattr:sock_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    
    Subsystem fs:
    allow fs.unconfined.typeattr fs.typeattr:blk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow fs.unconfined.typeattr fs.typeattr:chr_file { append audit_access create execute getattr ioctl link lock map open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow fs.unconfined.typeattr fs.typeattr:dir { add_name append audit_access create execute getattr ioctl link lock map mounton open quotaon read relabelfrom relabelto remove_name rename reparent rmdir search setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow fs.unconfined.typeattr fs.typeattr:fifo_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow fs.unconfined.typeattr fs.typeattr:file { append audit_access create execute execute_no_trans getattr ioctl link lock map mounton open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow fs.unconfined.typeattr fs.typeattr:filesystem { getattr mount quotaget quotamod relabelfrom relabelto remount unmount watch };
    allow fs.unconfined.typeattr fs.typeattr:lnk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow fs.unconfined.typeattr fs.typeattr:sock_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    
    Subsystem invalid:
    allow invalid.unconfined.typeattr invalid:alg_socket { accept append bind connect create getattr getopt ioctl listen lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:anon_inode { create getattr ioctl read };
    allow invalid.unconfined.typeattr invalid:appletalk_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:association { polmatch recvfrom sendto };
    allow invalid.unconfined.typeattr invalid:atmpvc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:atmsvc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:ax25_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:binder { call transfer };
    allow invalid.unconfined.typeattr invalid:blk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow invalid.unconfined.typeattr invalid:bluetooth_socket { accept append bind connect create getattr getopt ioctl listen lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:caif_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:can_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:chr_file { append audit_access create execute getattr ioctl link lock map open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow invalid.unconfined.typeattr invalid:dbus { acquire_svc send_msg };
    allow invalid.unconfined.typeattr invalid:dccp_socket { accept append bind connect create getattr getopt ioctl listen lock name_bind name_connect node_bind read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:decnet_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:dir { add_name append audit_access create execute getattr ioctl link lock map mounton open quotaon read relabelfrom remove_name rename reparent rmdir search setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow invalid.unconfined.typeattr invalid:fd use;
    allow invalid.unconfined.typeattr invalid:fifo_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow invalid.unconfined.typeattr invalid:file { append audit_access create execute execute_no_trans getattr ioctl link lock map mounton open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow invalid.unconfined.typeattr invalid:filesystem { getattr mount quotaget quotamod relabelfrom remount unmount watch };
    allow invalid.unconfined.typeattr invalid:icmp_socket { append bind connect create getattr getopt ioctl lock name_bind node_bind read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:ieee802154_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:infiniband_endport manage_subnet;
    allow invalid.unconfined.typeattr invalid:infiniband_pkey access;
    allow invalid.unconfined.typeattr invalid:ipc { associate create destroy getattr read setattr unix_read unix_write write };
    allow invalid.unconfined.typeattr invalid:ipx_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:irda_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:isdn_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:iucv_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:kcm_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:kernel_service { create_files_as use_as_override };
    allow invalid.unconfined.typeattr invalid:key { create link read search setattr view write };
    allow invalid.unconfined.typeattr invalid:key_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:llc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:lnk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow invalid.unconfined.typeattr invalid:msg { receive send };
    allow invalid.unconfined.typeattr invalid:msgq { associate create destroy enqueue getattr read setattr unix_read unix_write write };
    allow invalid.unconfined.typeattr invalid:netif { egress ingress };
    allow invalid.unconfined.typeattr invalid:netlink_audit_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_connector_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_crypto_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_dnrt_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_fib_lookup_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_generic_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_iscsi_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_kobject_uevent_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_netfilter_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_nflog_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_rdma_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_route_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_scsitransport_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_selinux_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_tcpdiag_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netlink_xfrm_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:netrom_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:nfc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:node { recvfrom sendto };
    allow invalid.unconfined.typeattr invalid:packet { forward_in forward_out recv send };
    allow invalid.unconfined.typeattr invalid:packet_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:passwd { chfn chsh crontab passwd rootok };
    allow invalid.unconfined.typeattr invalid:peer recv;
    allow invalid.unconfined.typeattr invalid:phonet_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:pppox_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:process { execmem fork getattr getcap getpgid getrlimit getsched getsession noatsecure ptrace rlimitinh setcap setcurrent setexec setfscreate setkeycreate setpgid setrlimit setsched setsockcreate share sigchld siginh sigkill signal signull sigstop };
    allow invalid.unconfined.typeattr invalid:qipcrtr_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:rawip_socket { append bind connect create getattr getopt ioctl lock name_bind node_bind read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:rds_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:rose_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:rxrpc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:sctp_socket { accept append association bind connect create getattr getopt ioctl listen lock name_bind name_connect node_bind read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:sem { associate create destroy getattr read setattr unix_read unix_write write };
    allow invalid.unconfined.typeattr invalid:service { disable enable reload start status stop };
    allow invalid.unconfined.typeattr invalid:shm { associate create destroy getattr lock read setattr unix_read unix_write write };
    allow invalid.unconfined.typeattr invalid:smc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:sock_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow invalid.unconfined.typeattr invalid:socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:system { halt reboot reload start status stop };
    allow invalid.unconfined.typeattr invalid:tcp_socket { accept append bind connect create getattr getopt ioctl listen lock name_bind name_connect node_bind read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:tipc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:tun_socket { append attach_queue bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:udp_socket { append bind connect create getattr getopt ioctl lock name_bind node_bind read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:unix_dgram_socket { append bind connect create getattr getopt ioctl lock read sendto setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:unix_stream_socket { accept append bind connect connectto create getattr getopt ioctl listen lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:vsock_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:x25_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow invalid.unconfined.typeattr invalid:xdp_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    
    Subsystem net:
    allow net.ib.endport.unconfined.typeattr net.ib.endport.typeattr:infiniband_endport manage_subnet;
    
    Subsystem net:
    allow net.ib.pkey.unconfined.typeattr net.ib.pkey.typeattr:infiniband_pkey access;
    
    Subsystem net:
    allow net.netif.unconfined.typeattr net.netif.typeattr:netif { egress ingress };
    
    Subsystem net:
    allow net.netnode.unconfined.typeattr net.netnode.typeattr:dccp_socket node_bind;
    allow net.netnode.unconfined.typeattr net.netnode.typeattr:icmp_socket node_bind;
    allow net.netnode.unconfined.typeattr net.netnode.typeattr:node { recvfrom sendto };
    allow net.netnode.unconfined.typeattr net.netnode.typeattr:rawip_socket node_bind;
    allow net.netnode.unconfined.typeattr net.netnode.typeattr:sctp_socket node_bind;
    allow net.netnode.unconfined.typeattr net.netnode.typeattr:tcp_socket node_bind;
    allow net.netnode.unconfined.typeattr net.netnode.typeattr:udp_socket node_bind;
    
    Subsystem net:
    allow net.packet.unconfined.typeattr net.packet.typeattr:packet { forward_in forward_out recv relabelto send };
    
    Subsystem net:
    allow net.peer.unconfined.typeattr net.peer.typeattr:peer recv;
    allow net.peer.unconfined.typeattr net.peer.typeattr:sctp_socket association;
    
    Subsystem net:
    allow net.port.unconfined.typeattr net.port.typeattr:dccp_socket { name_bind name_connect };
    allow net.port.unconfined.typeattr net.port.typeattr:icmp_socket name_bind;
    allow net.port.unconfined.typeattr net.port.typeattr:rawip_socket name_bind;
    allow net.port.unconfined.typeattr net.port.typeattr:sctp_socket { name_bind name_connect };
    allow net.port.unconfined.typeattr net.port.typeattr:tcp_socket { name_bind name_connect };
    allow net.port.unconfined.typeattr net.port.typeattr:udp_socket name_bind;
    
    Subsystem net:
    allow net.spd.unconfined.typeattr net.spd.typeattr:association { polmatch setcontext };
    
    Subsystem nodedev:
    allow nodedev.unconfined.typeattr nodedev.typeattr:chr_file { append audit_access create execute getattr ioctl link lock map open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    
    Subsystem procfile:
    allow procfile.unconfined.typeattr procfile.typeattr:dir { add_name append audit_access create execute getattr ioctl link lock map mounton open quotaon read remove_name rename reparent rmdir search setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow procfile.unconfined.typeattr procfile.typeattr:file { append audit_access create execute execute_no_trans getattr ioctl link lock map mounton open quotaon read rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow procfile.unconfined.typeattr procfile.typeattr:lnk_file { append audit_access create execute getattr ioctl link lock open quotaon read rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    
    Subsystem selinux:
    allow selinux.unconfined.typeattr selinux:security { check_context compute_av compute_create compute_member compute_relabel compute_user load_policy read_policy setbool setcheckreqprot setenforce setsecparam validate_trans };
    
    Subsystem stordev:
    allow stordev.unconfined.typeattr stordev.typeattr:blk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow stordev.unconfined.typeattr stordev.typeattr:chr_file { append audit_access create execute getattr ioctl link lock map open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    
    Subsystem subj:
    allow subj.unconfined.typeattr subj.entry.typeattr:file entrypoint;
    allow subj.unconfined.typeattr subj.typeattr:alg_socket { accept append bind connect create getattr getopt ioctl listen lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:anon_inode { create getattr ioctl read };
    allow subj.unconfined.typeattr subj.typeattr:appletalk_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:association { recvfrom sendto };
    allow subj.unconfined.typeattr subj.typeattr:atmpvc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:atmsvc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:ax25_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:binder { call impersonate set_context_mgr transfer };
    allow subj.unconfined.typeattr subj.typeattr:bluetooth_socket { accept append bind connect create getattr getopt ioctl listen lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:caif_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:can_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:dccp_socket { accept append bind connect create getattr getopt ioctl listen lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:decnet_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:dir { getattr ioctl lock open read search };
    allow subj.unconfined.typeattr subj.typeattr:fd use;
    allow subj.unconfined.typeattr subj.typeattr:fifo_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow subj.unconfined.typeattr subj.typeattr:file { append getattr ioctl lock open read write };
    allow subj.unconfined.typeattr subj.typeattr:icmp_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:ieee802154_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:ipc { associate create destroy getattr read setattr unix_read unix_write write };
    allow subj.unconfined.typeattr subj.typeattr:ipx_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:irda_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:isdn_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:iucv_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:kcm_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:kernel_service use_as_override;
    allow subj.unconfined.typeattr subj.typeattr:key { create link read search setattr view write };
    allow subj.unconfined.typeattr subj.typeattr:key_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:llc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:lnk_file { getattr lock read };
    allow subj.unconfined.typeattr subj.typeattr:msg { receive send };
    allow subj.unconfined.typeattr subj.typeattr:msgq { associate create destroy enqueue getattr read setattr unix_read unix_write write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_audit_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_connector_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_crypto_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_dnrt_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_fib_lookup_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_generic_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_iscsi_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_kobject_uevent_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_netfilter_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_nflog_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_rdma_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_route_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_scsitransport_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_selinux_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_tcpdiag_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netlink_xfrm_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:netrom_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:nfc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:packet_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:passwd { chfn chsh crontab passwd rootok };
    allow subj.unconfined.typeattr subj.typeattr:peer recv;
    allow subj.unconfined.typeattr subj.typeattr:perf_event { read write };
    allow subj.unconfined.typeattr subj.typeattr:phonet_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:pppox_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:process { dyntransition execheap execmem execstack fork getattr getcap getpgid getrlimit getsched getsession noatsecure ptrace rlimitinh setcap setcurrent setexec setfscreate setkeycreate setpgid setrlimit setsched setsockcreate share sigchld siginh sigkill signal signull sigstop transition };
    allow subj.unconfined.typeattr subj.typeattr:process2 nnp_transition;
    allow subj.unconfined.typeattr subj.typeattr:qipcrtr_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:rawip_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:rds_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:rose_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:rxrpc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:sctp_socket { accept append association bind connect create getattr getopt ioctl listen lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:sem { associate create destroy getattr read setattr unix_read unix_write write };
    allow subj.unconfined.typeattr subj.typeattr:shm { associate create destroy getattr lock read setattr unix_read unix_write write };
    allow subj.unconfined.typeattr subj.typeattr:smc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:tcp_socket { accept append bind connect create getattr getopt ioctl listen lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:tipc_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:tun_socket { append attach_queue bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:udp_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:unix_dgram_socket { append bind connect create getattr getopt ioctl lock read sendto setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:unix_stream_socket { accept append bind connect connectto create getattr getopt ioctl listen lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:vsock_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:x25_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    allow subj.unconfined.typeattr subj.typeattr:xdp_socket { append bind connect create getattr getopt ioctl lock read setattr setopt shutdown write };
    
    Subsystem sys:
    allow sys.unconfined.typeattr file.mod.typeattr:system module_load;
    allow sys.unconfined.typeattr sys.subj:system { ipc_info module_request syslog_console syslog_mod syslog_read };
    
    Subsystem sysfile:
    allow sysfile.unconfined.typeattr sysfile.typeattr:dir { add_name append audit_access create execute getattr ioctl link lock map mounton open quotaon read relabelfrom relabelto remove_name rename reparent rmdir search setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow sysfile.unconfined.typeattr sysfile.typeattr:file { append audit_access create execute execute_no_trans getattr ioctl link lock map mounton open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow sysfile.unconfined.typeattr sysfile.typeattr:lnk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom relabelto rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    
    Subsystem systemd:
    allow systemd.unconfined.typeattr file.unit.typeattr:service { disable enable reload start status stop };
    allow systemd.unconfined.typeattr fstab.file:service status;
    allow systemd.unconfined.typeattr sys.subj:service status;
    allow systemd.unconfined.typeattr sys.subj:system { halt reboot reload start status stop };
    allow systemd.unconfined.typeattr systemd.unit.run.file:service { disable enable reload start status stop };
    allow systemd.unconfined.typeattr user.systemd.home.conf.file:service { disable enable reload start status stop };
    allow systemd.unconfined.typeattr user.systemd.unit.run.file:service { disable enable reload start status stop };
    
    Subsystem tracefile:
    
    Subsystem unlabeled:
    allow unlabeled.unconfined.typeattr unlabeled:blk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow unlabeled.unconfined.typeattr unlabeled:chr_file { append audit_access create execute getattr ioctl link lock map open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow unlabeled.unconfined.typeattr unlabeled:dir { add_name append audit_access create execute getattr ioctl link lock map mounton open quotaon read relabelfrom remove_name rename reparent rmdir search setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow unlabeled.unconfined.typeattr unlabeled:fifo_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow unlabeled.unconfined.typeattr unlabeled:file { append audit_access create execute execute_no_trans getattr ioctl link lock map mounton open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow unlabeled.unconfined.typeattr unlabeled:kernel_service create_files_as;
    allow unlabeled.unconfined.typeattr unlabeled:lnk_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };
    allow unlabeled.unconfined.typeattr unlabeled:service { disable enable reload start status stop };
    allow unlabeled.unconfined.typeattr unlabeled:sock_file { append audit_access create execute getattr ioctl link lock open quotaon read relabelfrom rename setattr unlink watch watch_mount watch_reads watch_sb watch_with_perm write };


<a id="org215da79"></a>

## Domains

Domains associated with unconfined access to subsystems (if any).

    for typeattr in `seinfo dssp5-fedora/policy.* --flat -a \
    | grep unconfined.typeattr`
    do
        seinfo dssp5-fedora/policy.* --flat -xa $typeattr
        echo
    done

    attribute booleanfile.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute cgroupfile.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute dbus.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute debugfile.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute dev.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute file.unconfined.typeattr;
    	dracut.subj
    	sys.subj
    	sys.user.subj
    	systemd.tmpfiles.subj
    
    attribute fs.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute invalid.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute net.ib.endport.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute net.ib.pkey.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute net.netif.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute net.netnode.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute net.packet.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute net.peer.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute net.port.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute net.spd.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute nodedev.unconfined.typeattr;
    	systemd.tmpfiles.subj
    
    attribute procfile.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute selinux.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute stordev.unconfined.typeattr;
    	systemd.tmpfiles.subj
    
    attribute subj.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute sys.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute sysfile.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute systemd.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute tracefile.unconfined.typeattr;
    	sys.subj
    	sys.user.subj
    
    attribute unlabeled.unconfined.typeattr;
    	sys.subj
    	sys.user.subj


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Browse [`dssp5-fedora`](https://github.com/DefenSec/dssp5/tree/dssp5-fedora)

<sup><a id="fn.2" href="#fnr.2">2</a></sup> Browse [`dssp5`](https://github.com/DefenSec/dssp5)

<sup><a id="fn.3" href="#fnr.3">3</a></sup> About [`Access vectors`](https://github.com/SELinuxProject/selinux-notebook/blob/main/src/objects.md#object-classes-and-permissions)
